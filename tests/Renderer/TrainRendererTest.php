<?php
namespace BoardingCardsTest\Renderer;

use BoardingCards\Card;
use BoardingCards\Renderer;

class TrainRendererTest extends \PHPUnit_Framework_TestCase
{
    public function testTrainCanRenderTrainCard()
    {
        $card = new Card\TrainCard('New York', 'London', array('platform' => 'III'));
        $renderer = new Renderer\Train();
        $expected = 'Take train from New York to London, leaving from platform III';
        $this->assertEquals($expected, $renderer->render($card));
    }

    /**
     * Will fail as the renderer does not accept other than TrainCards
     *
     * @expectedException \PHPUnit_Framework_Error
     */
    public function testTrainWontRenderPlaneCard()
    {
        $card = new Card\PlaneCard('New York', 'London', array('gate' => '6B'));
        $renderer = new Renderer\Train();
        $renderer->render($card);
    }
}
