<?php
namespace BoardingCardsTest\Renderer;

use BoardingCards\Card;
use BoardingCards\Renderer\Plane;

class PlaneRendererTest extends \PHPUnit_Framework_TestCase
{
    public function testPlaneCanRenderPlaneCard()
    {
        $card = new Card\PlaneCard('New York', 'London', array('gate' => '6B'));
        $renderer = new Plane();
        $expected = 'Take plane from New York to London, leaving from gate 6B';
        $this->assertEquals($expected, $renderer->render($card));
    }

    /**
     * Will fail as the renderer does not accept other than PlaneCards
     *
     * @expectedException \PHPUnit_Framework_Error
     */
    public function testPlaneWontRenderTrainCard()
    {
        $card = new Card\TrainCard('New York', 'London', array('platform' => 'III'));
        $renderer = new Plane();
        $renderer->render($card);
    }
}
