<?php
namespace BoardingCardsTest;

use BoardingCards\Card\AbstractCard;
use BoardingCards\SortAdapter\SelectionSortAdapter;

class SelectionSortAdapterTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @dataProvider getCards()
     */
    public function testCanSortCards($cards, $expectedFromList, $expectedToList)
    {
        $cards = AbstractCard::factory($cards);
        $sorter = new SelectionSortAdapter();
        $actual = $sorter->sort($cards);
        $actualFromList = [];
        $actualToList = [];
        foreach ($actual as $card) {
            $actualFromList[] = $card->getFrom();
            $actualToList[] = $card->getTo();
        }
        $this->assertEquals($expectedFromList, $actualFromList);
        $this->assertEquals($expectedToList, $actualToList);
    }

    public function getCards()
    {
        return array(
            array(
                array(
                    array('plane', 'London', 'New York', array()),
                    array('plane', 'Paris', 'London', array()),
                    array('plane', 'New York', 'Tokio', array()),
                ),
                array('Paris', 'London', 'New York'),
                array('London', 'New York', 'Tokio'),
            ), array(
                array(
                    array('plane', 'London', 'New York', array()),
                ),
                array('London'),
                array('New York'),
            ), array(
                array( // no cards
                ),
                array(),
                array(),
            ), array(
                array(
                    array('plane', 'Tokio', 'Moscow', array()),
                    array('train', 'New York', 'Tokio', array()),
                    array('plane', 'London', 'New York', array()),
                    array('plane', 'Moscow', 'Dubai', array()),
                    array('plane', 'Paris', 'London', array()),
                ),
                array('Paris', 'London', 'New York', 'Tokio', 'Moscow'),
                array('London', 'New York', 'Tokio', 'Moscow', 'Dubai'),
            ), array(
                array(
                    array('plane', 'Tokio', 'Moscow', array()),
                    array('train', 'Prague', 'Washington', array()),
                    array('plane', 'New York', 'Tokio', array()),
                    array('plane', 'London', 'New York', array()),
                    array('train', 'Dubai', 'Prague', array()),
                    array('plane', 'Moscow', 'Dubai', array()),
                    array('plane', 'Paris', 'London', array()),
                ),
                array('Paris', 'London', 'New York', 'Tokio', 'Moscow', 'Dubai', 'Prague'),
                array('London', 'New York', 'Tokio', 'Moscow', 'Dubai', 'Prague', 'Washington'),
            )
        );
    }
}
