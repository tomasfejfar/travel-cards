<?php
namespace BoardingCardsTest;

use BoardingCards\Card;
use BoardingCards\Sorter;

class SorterTest extends \PHPUnit_Framework_TestCase
{
    public function testSortIsProxiedThroughAdapter()
    {
        $testData = array(
            new Card\TrainCard('London', 'Paris', array('platform' => '11')),
            new Card\TrainCard('Paris', 'Dubai', array('platform' => '3'))
        );

        $mock = $this->getMock('BoardingCards\SortAdapter\SortInterface', array('sort'));
        $mock->expects($this->once())
             ->method('sort')
             ->with($this->equalTo($testData));
        $sorter = new Sorter($mock);
        $sorter->sort($testData);
    }
}
