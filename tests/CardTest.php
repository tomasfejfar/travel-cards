<?php
namespace BoardingCardsTest;

use BoardingCards\Card\AbstractCard;

class CardTest extends \PHPUnit_Framework_TestCase
{
    public function testCanCreateCard()
    {

    }

    public function testCardFactoryCanCreateCards()
    {
        $data = array(
            array('train', 'London', 'Paris', array('platform' => '10')),
        );
        $cards = AbstractCard::factory($data);
        $this->assertCount(1, $cards);
        $this->assertInstanceOf('BoardingCards\Card\TrainCard', $cards[0]);
    }
}
