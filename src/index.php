<?php
require_once __DIR__ . '/../vendor/autoload.php';
// example workflow

/* data is an array - that allows for easy import from any format
 * json or yaml can by parsed to PHP array
 *
 */
$cardsInputData = include('cards-data.php');

/*
 * There is only once type of card object. It can be easily extended to add
 */
$cards = \BoardingCards\Card\AbstractCard::factory($cardsInputData);

/*
 * selection sort adapter is one of possible implementations
 * if there is a new adapter with more efficient algorithm it
 * can be injected
 */
$sorter = new \BoardingCards\Sorter(new \BoardingCards\SortAdapter\SelectionSortAdapter());
$sortedCards = $sorter->sort($cards);

$renderer = new \BoardingCards\Renderer\Strategy\Plain();
// $renderer = new \BoardingCards\Renderer\Strategy\PhpArray();
$result = $renderer->render($sortedCards);
$source = $renderer->render($cards);

echo 'Source:' . PHP_EOL;
echo $source . PHP_EOL;

echo 'Result:' . PHP_EOL;
echo $result . PHP_EOL;

