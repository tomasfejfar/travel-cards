<?php

namespace BoardingCards\SortAdapter;

use BoardingCards\Card\AbstractCard;

/**
 * Class SelectionSortAdapter
 *
 * sorts provided array using selection sort algorithm
 *
 * @package BoardingCards\SortAdapter
 */
class SelectionSortAdapter implements SortInterface
{
    /**
     * @param array $cards list of AbstractCard objects
     * @return array sorted list of AbstractCard objects
     */
    public function sort($cards)
    {
        if (!count($cards)) {
            return $cards;
        }

        $card = $initialCard = array_shift($cards);
        if (!$card) {
            return $cards;
        }
        $internalCards = [$card];
        while ($result = $this->findNextCard($card, $cards)) {
            array_push($internalCards, $result);
            $card = $result;
        } // last card
        $card = $initialCard;
        while ($result = $this->findPreviousCard($card, $cards)) {
            array_unshift($internalCards, $result);
            $card = $result;
        }

        return $internalCards;
    }

    /**
     * @param AbstractCard $mainCard card whose next card we are looking for
     * @param array $cards
     * @return mixed next AbstractCard or false if current is last
     */
    public function findNextCard(AbstractCard $mainCard, &$cards)
    {
        $currentTo = $mainCard->getTo();
        foreach ($cards as $id => $card) {
            if ($card->getFrom() == $currentTo) {
                unset($cards[$id]);
                return $card;
            }
        }
        return false; // card is the last one

    }

    /**
     * @param AbstractCard $mainCard card whose next card we are looking for
     * @param array $cards
     * @return mixed next AbstractCard or false if current is last
     */
    public function findPreviousCard(AbstractCard $mainCard, &$cards)
    {
        $currentFrom = $mainCard->getFrom();
        foreach ($cards as $id => $card) {
            if ($card->getTo() == $currentFrom) {
                unset($cards[$id]);
                return $card;
            }
        }
        return false; // card is the first one
    }
}
