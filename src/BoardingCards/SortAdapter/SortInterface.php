<?php

namespace BoardingCards\SortAdapter;

interface SortInterface
{
    public function sort($cards);
}
