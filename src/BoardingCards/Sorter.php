<?php

namespace BoardingCards;

use BoardingCards\SortAdapter\SortInterface;

/**
 * Class Sorter
 *
 * Used as proxy to sort adapter, allowing easily swapable adapters
 *
 * @package BoardingCards
 */
class Sorter
{
    protected $adapter;

    /**
     * @param SortInterface $adapter
     */
    function __construct(SortInterface $adapter)
    {
        $this->adapter = $adapter;
    }

    /**
     *
     *
     * @param array $cards array of AbstactCard objects
     * @return array sorted array
     */
    public function sort($cards)
    {
        return $this->adapter->sort($cards);
    }
}
