<?php

namespace BoardingCards\Renderer;

use BoardingCards\Card;

class PlainRenderer implements RendererInterface
{
    public function render(Card\CardInterface $card)
    {
        return sprintf(
            'Travel from %s to %s by %s',
            $card->getFrom(),
            $card->getTo(),
            $card->getType()
        );
    }
}
