<?php

namespace BoardingCards\Renderer;

use BoardingCards\Card\CardInterface;

class Plane implements RendererInterface
{

    public function render(CardInterface $card)
    {
        $data = $card->getData();
        return sprintf(
            'Take plane from %s to %s, leaving from gate %s',
            $card->getFrom(),
            $card->getTo(),
            $data['gate']
        );
    }
}
