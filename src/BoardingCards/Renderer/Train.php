<?php

namespace BoardingCards\Renderer;

use BoardingCards\Card\CardInterface;

class Train implements RendererInterface
{

    public function render(CardInterface $card)
    {
        $data = $card->getData();
        return sprintf(
            'Take train from %s to %s, leaving from platform %s',
            $card->getFrom(),
            $card->getTo(),
            $data['platform']
        );
    }
}
