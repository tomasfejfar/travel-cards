<?php

namespace BoardingCards\Renderer;

use BoardingCards\Card;

class PhpArrayRenderer implements RendererInterface
{
    public function render(Card\CardInterface $card)
    {
        return array(
            $card->getType(),
            $card->getFrom(),
            $card->getTo(),
            $card->getData(),
        );
    }
}
