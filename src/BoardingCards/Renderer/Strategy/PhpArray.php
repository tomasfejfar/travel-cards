<?php

namespace BoardingCards\Renderer\Strategy;

use BoardingCards\Card;
use BoardingCards\Renderer\PhpArrayRenderer;

class PhpArray
{
    function __construct()
    {
        $this->renderer = new PhpArrayRenderer();
    }

    public function render($cards)
    {
        $result = [];
        foreach ($cards as $card) {
            array_push($result, $this->renderer->render($card));
        }
        return $result;
    }
}
