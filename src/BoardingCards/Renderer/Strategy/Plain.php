<?php

namespace BoardingCards\Renderer\Strategy;

use BoardingCards\Card;
use BoardingCards\Renderer\PlainRenderer;

class Plain
{
    function __construct()
    {
        $this->renderer = new PlainRenderer();
    }

    public function render($cards)
    {
        $result = [];
        foreach ($cards as $card) {
            $result[] = $this->renderer->render($card);
        }
        return implode(PHP_EOL, $result);
    }
}
