<?php

namespace BoardingCards\Renderer;

use BoardingCards\Card;

interface RendererInterface
{
    public function render(Card\CardInterface $card);
}
