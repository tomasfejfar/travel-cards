<?php
namespace BoardingCards\Card;

class PlaneCard extends AbstractCard
{
    const TYPE = 'plane';

    /**
     * @param string $from place from where the card is valid
     * @param string $to place to where the card is valid
     * @param mixed $data any data that may be needed (and will be understood by Renderer)
     * @throws \InvalidArgumentException
     */
    function __construct($from, $to, $data)
    {
        return parent::__construct(self::TYPE, $from, $to, $data);
    }
}
