<?php
namespace BoardingCards\Card;


interface CardInterface
{
    public function getFrom();
    public function getTo();
    public function getType();
    public function getData();
}
