<?php
namespace BoardingCards\Card;

abstract class AbstractCard implements CardInterface
{
    const TYPE_PLANE = 'plane';
    const TYPE_TRAIN = 'train';

    protected $from;
    protected $to;
    protected $data;
    protected $type;

    public static function factory($cardsPlainArray)
    {
        $result = [];
        foreach ($cardsPlainArray as $rowId => $cardData) {
            if (count($cardData) != 4) {
            }
            list($type, $from, $to, $data) = $cardData;
            $testClass = 'BoardingCards\\Card\\' . ucfirst($type) . 'Card';
            if (class_exists($testClass)) {
                $class = $testClass;
            } else {
                throw new \DomainException(sprintf('Cannot work with "%s" card (row %s) - there is no "%s" class', $type, $rowId, $testClass));
            }
            $result[] = new $class($from, $to, $data);
        }
        return $result;
    }

    protected function setType($type)
    {
        if (in_array($type, array(self::TYPE_PLANE, self::TYPE_TRAIN))) {
            $this->type = $type;
        } else {
            throw new \InvalidArgumentException(sprintf('Invalid type "%s"', $type));
        }
    }

    /**
     * @param string $type card type
     * @param string $from place from where the card is valid
     * @param string $to place to where the card is valid
     * @param mixed $data any data that may be needed (and will be understood by Renderer)
     * @throws \InvalidArgumentException
     */
    function __construct($type, $from, $to, $data)
    {
        $this->setType($type);
        $this->from = $from;
        $this->to = $to;
        $this->data = $data;
    }

    /**
     * @return array
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @return string
     */
    public function getFrom()
    {
        return $this->from;
    }

    /**
     * @return string
     */
    public function getTo()
    {
        return $this->to;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }
} 
