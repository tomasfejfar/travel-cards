<?php
return array(
    array('plane', 'London', 'New York', array('gate' => '10B')),
    array('train', 'Paris', 'London', array('platform' => '3')),
    array('plane', 'Moscow', 'Prague', array('game' => '8A')),
    array('train', 'Prague', 'Paris', array('platform' => '7')),
);
